#include <pthread.h>
#include <stdlib.h>

#include <olf_config.h>
#include <olf_buffer.h>
#include <olf_player.h>
#include <olf_platforms.h>
#include <olf_functions.h>

void *play_olf(void *arg);

int main(int argc, char **argv)
{
	pthread_t players[2];
    platform_init(&olf_functions, argc, argv);
	OLF_IDLE();

	dbg("Current time (in ms) is %llu\n", OLF_TIME_MS());

	pthread_create(&players[0], NULL, (void*)&play_olf, "Scratchy");
	pthread_create(&players[1], NULL, (void*)&play_olf, "Itchy");

    if(pthread_join(players[0], NULL))
		dbg("Game Over!");

	return 0;
}

void *play_olf(void *arg)
{
	int lives = 10;
	int ammo = 100;
	const char *pname = (char *) arg;
	const char *p2name = "Shooter";
	struct player ply;
	struct player ply2;
	player_init(&ply, &pname, lives, ammo);
	
	player_init(&ply2, &p2name, lives, ammo);
	
	srand(time(NULL));
	while(ply.num_lives > 0)
	{
		usleep(500000);
		if((rand() % 20 + 1) == 1)
		{	
			player_is_shot(&ply, &ply2);
		}
		else
			player_shoot(&ply);		
	}
	return NULL;
}
