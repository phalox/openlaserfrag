#include <stdio.h>

#include <check.h>
/* Including the C files (not the H files):
 * - Allows testing of static functions
 * - Checks for namespace collisions
 */
#include <olf_buffer.c>
#include <olf_player.c>

START_TEST (buffer_add_to_full)
{
	int my_element = 123;
	int ret = 0;
	int buf_size = 10;	
	int j = 0;
	struct buffer buf;
	dbg("Testing add to full *******************\n");
	buffer_init(&buf, buf_size);

	//check if we can add the number of elements which equals to the size
	while(j < buf_size)
	{
		buffer_put(&buf, &my_element);
		j ++;
		fail_unless(ret == 0, "The buffer failed to accept data while it is not full");
	}
	//Add even more
	ret = buffer_put(&buf, &my_element);
	fail_unless(ret == -1, "Putting an entry in the full buffer");
	buffer_destroy(&buf);
	dbg("Finished testing add to full ****************\n");
}
END_TEST

START_TEST (buffer_pop_to_empty)
{
	int my_element = 123;
	int *my_ret_element;
	int ret = 0;
	int buf_size = 10;
	int i = 0, j = 0;
	struct buffer buf;
	dbg("Testing pop to empty **********\n");
	buffer_init(&buf, buf_size);

	//add to full
	while(j < buf_size)
	{
		buffer_put(&buf, &my_element);
		j ++;
	}

	//pop until empty
	i = buf_size;	
	while(i > 0)
	{
		ret = buffer_pop(&buf, (void **)&my_ret_element);
		fail_unless(ret == 0, "Popping a buffer with  the element number %d did not work", i);
		i --;
	}
	
	//Try to pop even more
	ret = buffer_pop(&buf, (void **)&my_ret_element);
	fail_unless(ret == -1, "Popping an empty buffer.");
	buffer_destroy(&buf);
	dbg("Finished testing pop to empty ***********\n");
}
END_TEST

START_TEST (buffer_put_and_pop)
{
	int *my_ret_element;
	int ret = 0;
	int element = 234;
	int buf_size = 10;
	struct buffer buf;
	int i = 0;
	dbg("Testing put and pop ***********\n");
	buffer_init(&buf, buf_size);

	//adding 5 elements
	while(i < 5)
	{	
		buffer_put(&buf, &element);
		i ++;
	}

	i = 5;
	//"removing 5 elements.. 
	while(i > 0)
	{
		ret = buffer_pop(&buf, (void **)&my_ret_element);
		fail_unless(ret == 0, "Popping a buffer with  the element number %d did not work", i);
		i --;
	}

	i = 0;
	printf("adding 10 elements.. \n");
	while(i < buf_size)
	{
		//printf("adding %d \n", i);
		buffer_put(&buf, &element);
		i ++;	
	}
	i = buf_size;
	printf("removing 10 elements.. \n");
	while(i > 0)
	{
		ret = buffer_pop(&buf, (void **)&my_ret_element);
		fail_unless(ret == 0, "Popping a buffer with  the element number %d did not work", i);
		//printf(" i = %d\n", i);
		i --;
	}
	buffer_destroy(&buf);
	dbg("Finish testing put and pop ***********\n");
}
END_TEST

START_TEST (buffer_add_entry)
{
	int my_element = 123;
	int *my_ret_element;
	int ret = 0;
	int buf_size = 10;
	
	struct buffer buf;
	dbg("Testing add element ***********\n")
	buffer_init(&buf, buf_size);

	ret = buffer_put(&buf, &my_element);
	fail_unless(ret == 0, "Failed to put data in an empty buffer.");

	buffer_pop(&buf, (void **)&my_ret_element);
	ret = buffer_put(&buf, &my_element);
	fail_unless(ret == 0, "The buffer failed to accept data when its data was removed");
	
	ret = buffer_put(NULL, &my_element);
	fail_unless(ret == -1, "The buffer accepted an empty buffer pointer");

	ret = buffer_put(&buf, NULL);
	fail_unless(ret == -1, "The buffer accepted an empty element");

	/*
	for(i = 0; i < buf_size + 1; i++)
	{
		ret = buffer_put(&buf, &my_element);	
		if(i >= buf_size)
		{
			fail_unless(ret == -1, "Buffer accepted element while full");		
		}
	}*/

	buffer_destroy(&buf);
	dbg("finished testing add element ***********\n");
}
END_TEST

START_TEST (buffer_remove_entry)
{
	int *my_ret_element;
	int ret = 0;
	int element = 234;
	struct buffer buf;
	dbg(" Testing remove entry ***********\n");
	buffer_init(&buf, 10);
	
	ret = buffer_pop(&buf, (void **)&my_ret_element);
	fail_unless(ret == -1, "The buffer popped while being empty!");
	
	buffer_put(&buf, &element);
	ret = buffer_pop(&buf, (void **)&my_ret_element);
	fail_unless(ret == 0, "Pop the buffer with one element did not work! ");
	
	ret = buffer_pop(&buf, (void **)&my_ret_element);
	fail_unless(ret == -1, "The buffer popped while being empty!");	

	ret = buffer_pop(NULL, (void **)&my_ret_element);
	fail_unless(ret == -1, "The buffer popped after added the element and then popped once");

	ret = buffer_pop(NULL, (void **)&my_ret_element);
	fail_unless(ret == -1, "The buffer accepted an empty buffer pointer");

	ret = buffer_pop(&buf, NULL);
	fail_unless(ret == -1, "The buffer accepted an empty element pointer");
	
	ret = buffer_pop(&buf, (void **) my_ret_element);
	fail_unless(ret == -1, "The buffer popped while being empty");

	buffer_destroy(&buf);
	dbg("Finish testing remove entry ***********\n");
}
END_TEST

START_TEST (buffer_regular_entry)
{
	int my_element = 123;
	int *my_ret_element = NULL;
	int ret = 0;
	struct buffer buf;
	buffer_init(&buf, 10);

	ret = buffer_put(&buf, &my_element);
	fail_unless(ret == 0, "Putting an entry in the buffer did not work");
	
	ret = buffer_pop(&buf, (void **)&my_ret_element);
	fail_unless(ret == 0, "Popping an entry in the buffer did not work");
	fail_unless(&my_element == my_ret_element, "The pointer that was put is not the same as the popped one");

	buffer_destroy(&buf);
}
END_TEST

START_TEST (player_shoot_test)
{
    int ret = 0;

    const char *pname = "OLF Guy";
    int lives = 10;
    int ammo = 100;
    struct player ply;
    player_init(&ply, &pname, lives, ammo);

    ret = player_shoot(NULL);
    fail_unless(ret == -1, "player_shoot accepted NULL pointer for player");

    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot did not accept valid player pointer");

    ply.num_lives = 0;
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when lives = 0");

    ply.num_lives = lives;
    ply.ammo = 0;
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when ammo = 0");

    ply.num_lives = lives;
    ply.ammo = ammo;
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when ammo > 0 and lives > 1");

    ply.last_hit = OLF_TIME_MS() - HIT_COOLDOWN_MS - 100;
    ply.last_shot = OLF_TIME_MS();
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when hit cooldown was met");

    ply.last_shot = OLF_TIME_MS() - SHOOT_COOLDOWN_MS - 100;
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when shoot and hit cooldown was met");

    ply.last_hit = OLF_TIME_MS();
    ply.last_shot = OLF_TIME_MS() - SHOOT_COOLDOWN_MS - 100;
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when shoot cooldown was met");

    ply.last_hit = OLF_TIME_MS();
    ply.last_shot = OLF_TIME_MS();
    ret = player_shoot(&ply);
    fail_unless(ret == 0, "player_shoot failed when shoot and hit cooldown was NOT met");

}
END_TEST

START_TEST (player_is_shot_test)
{
    int ret = 0;

    const char *pname = "Itchy";
    const char *p2name = "Scratchy";
    int lives = 10;
    int ammo = 100;
    struct player ply;
    struct player ply2;
    player_init(&ply, &pname, lives, ammo);

    player_init(&ply2, &p2name, lives, ammo);

    ret = player_is_shot(NULL, NULL);
    fail_unless(ret == -1, "player_is_shot accepted NULL pointers");

    ret = player_is_shot(&ply, NULL);
    fail_unless(ret == -1, "player_is_shot accepted NULL pointers");

    ret = player_is_shot(NULL, &ply2);
    fail_unless(ret == -1, "player_is_shot accepted NULL pointers");

    ret = player_is_shot(&ply, &ply2);
    fail_unless(ret == 0, "player_is_shot did not accept valid player pointer");

    ply.num_lives = 1;
    ret = player_is_shot(&ply, &ply2);
    fail_unless(ret == 0, "player_is_shot failed with num_lives = 1");

    ply.num_lives = 0;
    ret = player_is_shot(&ply, &ply2);
    fail_unless(ret == 0, "player_is_shot failed with num_lives = 0");

}
END_TEST

START_TEST (player_dies_test)
{
    int ret = 0;

    const char *pname = "Itchy";
    int lives = 10;
    int ammo = 100;
    struct player ply;
    player_init(&ply, &pname, lives, ammo);

    ret = player_dies(NULL);
    fail_unless(ret == -1, "player_dies accepted NULL pointer");

    ret = player_dies(&ply);
    fail_unless(ret == -1, "player_dies failed with 10 lives");

    ply.num_lives = 0;
    ret = player_dies(&ply);
    fail_unless(ret == 0, "player_dies failed with valid pointer");

}
END_TEST


Suite *olf_suite(void)
{
	Suite *s;
	TCase *tc;
	s = suite_create("OpenLaserFrag");

	tc = tcase_create("Buffer test");
	tcase_add_test(tc, buffer_add_to_full);
	tcase_add_test(tc, buffer_pop_to_empty);
	tcase_add_test(tc, buffer_put_and_pop);
	tcase_add_test(tc, buffer_add_entry);
	tcase_add_test(tc, buffer_remove_entry);
	tcase_add_test(tc, buffer_regular_entry);
	suite_add_tcase(s, tc);

	tc = tcase_create("Buffer test");
	tcase_add_test(tc, player_shoot_test);
	tcase_add_test(tc, player_is_shot_test);
	tcase_add_test(tc, player_dies_test);
	suite_add_tcase(s, tc);
	return s;
}

int main(void)
{
	int fails;
	Suite *s = olf_suite();
	SRunner *sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	fails = srunner_ntests_failed(sr);
	srunner_free(sr);
	return fails;
}
