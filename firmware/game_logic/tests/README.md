# Unit testing the firmware 

We are making use of the **check** framework to unit test all of our code.
The version of this framework available in the repo's is very old, so please download the most recent version here:
http://sourceforge.net/projects/check/files/check/

+ Extract the files somewhere in your documents folder
+ Open a terminal and go to this folder
+ Do a sudo ./configure
+ Do a sudo make install
+ This framework should be installed now!
+ By adding the -icheck parameter to your compile options, you can include its functionality


## Running the test
+ Compile the tests (from the game_logic folder): gcc -Wall tests/test_buffer.c components/buffer.c -otest -Iinclude -lcheck
+ Run them: ./test
