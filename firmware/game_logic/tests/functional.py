#!/usr/bin/python

import os, tempfile, time

def makeAnotherFifo(directory):
    tmpfile = tempfile.mktemp(dir=directory)
    inputFifoName = tmpfile + "_i"
    outputFifoName = tmpfile + "_o"
    print "New Fifo_i: {0}, Fifo_o: {1}".format(inputFifoName, outputFifoName)
    return inputFifoName
    

tmpdir = tempfile.mkdtemp(prefix="olf_")

while True :
    filename = makeAnotherFifo(tmpdir)
    time.sleep(1)

exit(0)

try:
    os.mkfifo(filename)
except OSError, e:
    print "Failed to create FIFO: %s" % e
else:
    # No buffering read
    #fifo_read = open('fifo', 'r', 0)
    fifo = open(filename, 'w')
    # write stuff to fifo
    print >> fifo, "hello"
    fifo.close()
    os.remove(filename)
    os.rmdir(tmpdir)

