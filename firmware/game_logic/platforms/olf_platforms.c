#include <olf_platforms.h>
#include <olf_config.h>
#include <olf_functions.h>


/*
* Add new platform structs here!
*/

#ifdef OLF_PLATFORM_SIM
#include <simulated/functions.h>
struct platform olf_platform = 
{
	.init = &simulated_init,
	.destroy = &simulated_destroy,
	//.some_event = NULL; // Event pointer is not used!
};
#elif defined OLF_PLATFORM_PHALOX
struct platform olf_platform = 
{
	.init = NULL;
};
#else
# error No platform defined
#endif

/*
* This is common code for all platforms, please don't touch
*/

int platform_init(struct functions *fcn, int argc, char **argv)
{
	if(olf_platform.init)
		return olf_platform.init(fcn, argc, argv);
	else
		return -1;
}
