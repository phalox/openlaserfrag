#include <stdio.h>

#include <pthread.h>
#include <stdlib.h>
#include "buffer.h"


struct buffer myBuffer;

void* thread_put(void* data)
{
	
	int p = *(int*)data;
	int ret = 0;
	ret = buffer_put(&myBuffer, &p);
	
	ret = buffer_put(&myBuffer, &p);
	
}


void* thread_pop(void* data)
{	
	int ret = 0;
	ret = buffer_pop(&myBuffer, &data);
	//printf("The data I poped is: %d\n", *(int*)data);
	printf("The data I poped is: %s\n", (char*)data);
}


int main(int argc, char **argv)
{
	printf("here it entering the main function\n");

	buffer_init(&myBuffer, 10);

	int data = 2;
	
	char* data3 = "abc";
	int* data_out;
	char* data4;
	
	
	buffer_put(&myBuffer, data3);
	buffer_pop(&myBuffer, (void **)&data4);
	
	printf("data4 is %s\n", data4);

	
	pthread_t thread_handle_put;
	pthread_t thread_handle_pop;
	int ret1 = pthread_create(&thread_handle_put,0,thread_put,(void*)&data);
	int ret2 = pthread_create(&thread_handle_pop,0,thread_pop,(void*)data_out);
	
	
	
	if(ret1 != 0)
	{
		printf("Create thread put faied! error : %d", ret1);
		return 1;	
	}

	
	if(ret2 != 0)
	{
		printf("Create thread pop faied! error : %d", ret1);
		return 1;	
	}

	pthread_join(thread_handle_put, 0);
	pthread_join(thread_handle_pop, 0);

	return 0;
}
