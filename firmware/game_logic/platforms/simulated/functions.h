#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__
#include <olf_platforms.h>
#include <olf_config.h>

int simulated_init(struct functions *, int argc, char **argv);
int simulated_destroy(struct functions *);

#endif
