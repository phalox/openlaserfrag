#include "functions.h"
#include <stdio.h>
#include <olf_config.h>

int simulated_init(struct functions *fcn, int argc, char **argv)
{
    printf("Init called with %d arguments\n", argc);
	return 0;
}

int simulated_destroy(struct functions *fcn)
{
	return 0;
}
