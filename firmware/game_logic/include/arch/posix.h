#ifndef OLF_SUPPORT_POSIX
#define OLF_SUPPORT_POSIX

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

# define dbg(...) printf(__VA_ARGS__);
//# define dbg(...) ;

# define olf_zalloc(x) calloc(x, 1)
# define olf_free(x) free(x)

static inline unsigned long long OLF_TIME(void)
{
	struct timeval t;
	gettimeofday(&t, NULL);
	return (t.tv_sec);
}

static inline unsigned long long OLF_TIME_MS(void)
{
	struct timeval t;
	gettimeofday(&t, NULL);
	return (t.tv_sec * 1000LL) + (t.tv_usec / 1000LL);
}

static inline void OLF_IDLE(void)
{
	usleep(10000);
}

#endif

