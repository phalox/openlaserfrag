#ifndef __OLF_PLATFORMS_H__
#define __OLF_PLATFORMS_H__

#include <olf_functions.h>

struct platform
{
	int (*init)(struct functions *, int argc, char **argv);
	int (*destroy)(struct functions *);
	//some_event_ptr some_event;
};

int platform_init(struct functions *, int argc, char **argv);

#endif
