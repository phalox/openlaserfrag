#ifndef __OLF_CONFIG_H__
#define __OLF_CONFIG_H__

// Temporarily defining the arch and platform here
// Has to move into the makefiles!
#define OLF_ARCH_POSIX
#define OLF_PLATFORM_SIM

#ifdef OLF_ARCH_POSIX
# include <arch/posix.h>
#elif defined OLF_ARCH_SOMETHING_ELSE 
// Include some other arch
#elif defined OLF_ARCH_SOMETHINGELSE
# error There is no architecture defined
#endif

#endif
