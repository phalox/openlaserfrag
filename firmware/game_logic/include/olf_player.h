#ifndef __OLF_PLAYER_H__
#define __OLF_PLAYER_H__

#include <olf_config.h>

#define SHOOT_COOLDOWN_MS 500
#define HIT_COOLDOWN_MS 1000

struct player {
	const char **name;
	int num_lives; 	/* Times a player can be shot before dying, 0 == DEAD */
	int points;		/* Amount of points amassed by player */
	int ammo;		/* Amount of ammo the player has */
	long long unsigned int last_shot;	/* OLF Time when the last shot was fired */
	long long unsigned int last_hit;	/* OLF Time when player was last hit */
/*	int team_id;	   ID of the team of the player, 0 = no team */	    
};


int player_init(struct player *player, const char **name, int lives, int ammo);
int player_destroy(struct player *player);
int player_shoot(struct player *player);
int player_is_shot(struct player *player, struct player *shooter);
int player_dies(struct player *player);

/* Stuff to add in a later iteration
int player_reload(struct player *player);
*/

#endif
