#ifndef __OLF_BUFFER_H__
#define __OLF_BUFFER_H__

#include <olf_config.h>
#include <stdlib.h>

struct buffer {
	int size;
	int head;
	int tail;
	void **data; // has to be malloced beforehand
};

/*
* buffer_init initializes a buffer struct with the number of elements
*/
int buffer_init(struct buffer *buffer, unsigned int buffer_size);

/*
* buffer_destroy frees the buffer memory, but it does NOT free the stored elements
*/
int buffer_destroy(struct buffer *buffer);
int buffer_put(struct buffer *buffer, void *element);
int buffer_pop(struct buffer *buffer, void **element);
int buffer_peek(struct buffer *buffer, void **element);

#endif
