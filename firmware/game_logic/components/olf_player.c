#include <olf_config.h>
#include <olf_player.h>

int player_init(struct player *player, const char **name, int lives, int ammo)
{
	if(!player)
	{
		dbg("Player cannot be a null pointer\n");
		return -1;
	}
	if(!name)
	{
		dbg("Player name cannot be a null pointer\n");
		return -1;
	}
	if(lives <= 0)
	{
		dbg("Player lives cannot be equal to or less than zero\n");
		return -1;
	}
	if(ammo <= 0)
	{
		dbg("Player ammo cannot be equal to or less than zero\n");
		return -1;
	}
	
	player->name = name;
	player->num_lives = lives;
	player->ammo = ammo;

	return 0;	
}

int player_destroy(struct player *player)
{
	return -1; // Needs implementation
}

int player_shoot(struct player *player)
{
	 if(!player)
	 {
		 dbg("Player is NULL pointer in player_shoot\n");
		 return -1;
	 }
	 if(player->num_lives < 1)
	 {
		 dbg("%s is dead, can't shoot!\n", *player->name);
		 return 0;
	 } 
	 if(OLF_TIME_MS() - player->last_hit > HIT_COOLDOWN_MS && OLF_TIME_MS() - player->last_shot > SHOOT_COOLDOWN_MS)
	 {
		 /* Cooldowns OK, update last shot time and continue */
		 player->last_shot = OLF_TIME_MS();
	 }
	 else
	 {
		 dbg("%s can't shoot yet, cooldown not finished!\n", *player->name);
		 return 0;		 
	 }
	 if(player->ammo > 0)
	 {
		 /* Send the shoot signal */
		 dbg("%s shoots.. Pew pew!\n", *player->name);
		 return 0;
	 }
	 else
	 {
		 dbg("%s has no ammo left! Do a reload!\n", *player->name);
		 return 0;
	 }
}

int player_is_shot(struct player *player, struct player *shooter)
{
	 if(!player || !shooter)
	 {
		 dbg("Player and/or shooter are NULL pointers in player_is_shot\n");
		 return -1;
	 }
	 player->last_hit = OLF_TIME_MS();
	 if(player->num_lives > 1)
	 {
		 player->num_lives--;
		 dbg("%s got shot by %s and has %d lives left\n", *player->name, *shooter->name, player->num_lives);
		 return 0;
	 }
	 else
	 {
		 dbg("%s killed %s\n", *shooter->name, *player->name);
		 player->num_lives = 0; /* Player is dead */
		 player_dies(player);
		 return 0;
	 }
}

int player_dies(struct player *player)
{
	 if(!player)
	 {
		 dbg("Player is NULL pointer in player_dies\n");
		 return -1;
	 }
	 if(player->num_lives > 0)
	 {
		 return -1; /* Should never happen! */
	 }
	 /* Do player dying stuff here */
	 return 0; 
}


