#include <olf_config.h>
#include <olf_buffer.h>

int buffer_init(struct buffer *buffer, unsigned int buffer_size)
{
	if(!buffer)
	{
		dbg("Buffer cannot be a null pointer\n");
		return -1;
	}

	if(buffer_size == 0)
	{
		dbg("Buffer size should be greater than 0 elements\n");
		return -1;
	}

	buffer->data = malloc(buffer_size * sizeof(buffer->data));
	if(!buffer->data)
	{
		dbg("Allocating memory on the heap failed");
		return -1;
	}
	buffer->size = buffer_size;
	buffer->head = buffer->tail = 0;
	return 0;
}

int buffer_destroy(struct buffer *buffer)
{
    if(!buffer)
    {
        dbg("Buffer cannot be a null pointer\n");
        return -1;
    }

	if(!buffer->data)
	{
		dbg("Something happend to the buffer. Memory corrupted!\n");
		return -1;
	}

	free(buffer->data);
	buffer->head = buffer->tail = buffer->size = 0;
	return 0;
}

int buffer_put(struct buffer *buffer, void *element)
{
	int next_head;
	if(!buffer || !element)
	{
		dbg("Buffer and/or element are NULL pointers in buffer_put\n");
		return -1;
	}
	next_head = (buffer->head+ 1)%(buffer->size + 1) ;
	if (next_head != buffer->tail )
	{
		/* there is room */
		buffer->data[buffer->head] = (void *)element;
		buffer->head = next_head;
		return 0;
	} 
	else 
	{
		/* no room left in the buffer */		
		dbg("Buffer is full\n");
		return -1;
	}
}

int buffer_pop(struct buffer *buffer, void **element)
{
	if(!buffer || !element)
	{
		dbg("Buffer and/or element are NULL pointers in buffer_pop\n");
		return -1;
	}

	if (buffer->head != buffer->tail)
	{
		*element = buffer->data[buffer->tail];
		buffer->tail = (buffer->tail + 1)%(buffer->size + 1);
		return 0;
	}
	else
	{
		dbg("There are no elements in the buffer\n");
		return -1;
	}
}

int buffer_peek(struct buffer *buffer, void **element)
{
	if(!buffer || !element)
	{
		dbg("Buffer and/or element are NULL pointers in buffer_peek\n");
		return -1;
	}
	if (buffer->head != buffer->tail)
	{
		*element = buffer->data[buffer->tail];
		return 0;
	}
	else
	{
		dbg("There are no elements in the buffer\n");
		return -1;
	}	
}

