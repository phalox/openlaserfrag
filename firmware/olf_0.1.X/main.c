/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/
#define __18CXX
#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#include <p18f2550.h>

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include <delays.h>

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

#define LED_1              LATCbits.LATC0
#define LED_2              LATCbits.LATC1

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

void main(void)
{
    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    InitApp();

    // Default all pins to digital
    ADCON1 |= 0x0F;

    // Start out both LEDs off
    LED_1 = 0;
    LED_2 = 0;

    // Make both LED I/O pins be outputs
    TRISCbits.TRISC0 = 0;
    TRISCbits.TRISC1 = 0;

    while(1)
    {
        // Alternate LEDs
        LED_1 = 1;
        LED_2 = 1;
        Delay10KTCYx(250);

        LED_1 = 0;
        LED_2 = 0;
        Delay10KTCYx(100);
    }

}

