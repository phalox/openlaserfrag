/*****************************************************
		Template File
**************************************************** */


/*****************************************************
		Includes
**************************************************** */

#include <p18cxxx.h>
#include <delays.h> //Enables you to add varying amount of NOPs (Delays)
#include <stdlib.h>
#include <stdio.h>

/*****************************************************
		        Defines
**************************************************** */

#define LOWER_BYTE(x) ((x) & 0xFF)
#define UPPER_BYTE(x) ((x) >> 8)
#define ALL_BYTE(high, low) ((high) << 8) | (low)

#define PULSE_LEN_2400  225
#define PULSE_LEN_1200  112
#define PULSE_LEN_600   56

#define TIM0_PRELOAD_MAX    255
#define TIM0_PRELOAD_2400   (TIM0_PRELOAD_MAX - PULSE_LEN_2400)
#define TIM0_PRELOAD_1200   (TIM0_PRELOAD_MAX - PULSE_LEN_1200)
#define TIM0_PRELOAD_600    (TIM0_PRELOAD_MAX - PULSE_LEN_600)

#define TIM1_PRELOAD_MAX    65536
#define TIM1_PRELOAD_2500   (TIM1_PRELOAD_MAX - 30018)
#define TIM1_PRELOAD_1300   (TIM1_PRELOAD_MAX - 15609)
#define TIM1_PRELOAD_700    (TIM1_PRELOAD_MAX - 8405)

#define TIM1_2450           TIM1_PRELOAD_2500 + 29418
#define TIM1_2350           TIM1_PRELOAD_2500 + 28217
#define TIM1_1250           TIM1_PRELOAD_1300 + 15009
#define TIM1_1150           TIM1_PRELOAD_1300 + 13808
#define TIM1_650            TIM1_PRELOAD_700 + 7805
#define TIM1_550            TIM1_PRELOAD_700 + 6604

#define IOC_PIN_MASK        0b00010000

#define PWM_ON              CCP1CONbits.CCP1M = 0b1100;
#define PWM_OFF             CCP1CONbits.CCP1M = 0b0000;

#define PLM_ON              T0CONbits.TMR0ON = 1;
#define PLM_OFF             T0CONbits.TMR0ON = 0;

#define RCV_TIMER_ON        T0CONbits.TMR0ON = 1;
#define RCV_TIMER_OFF       T0CONbits.TMR0ON = 0;

#define TRIGGER_T0_INT      INTCONbits.TMR0IF = 1;

#define RISING_EDGE_CHECK   INTCON3bits.INT1IF == 1
#define RISING_EDGE_RESET   INTCON3bits.INT1IF = 0;
#define FALLING_EDGE_CHECK  INTCONbits.INT0IF == 1
#define FALLING_EDGE_RESET  INTCONbits.INT0IF = 0;
#define RCV_TIMER_CHECK     PIR1bits.TMR1IF == 1
#define RCV_TIMER_RESET     PIR1bits.TMR1IF = 0;

enum ir_transmit_state_e
{
  IDLE_TRANSMIT,
  SEND_HEADER,
  SEND_SPACE,
  SEND_BIT,
  FINISH_TRANSMIT
};

enum ir_receive_state_e
{
  IDLE_RECEIVE,
  START_HDR_TIMER,
  CHECK_HDR_START_PAUSE_TIMER,
  CHECK_PAUSE_START_BIT_TIMER,
  CHECK_BIT_START_PAUSE_TIMER,
  ERROR_RECEIVE,
  FINISH_RECEIVE
};

/*****************************************************
		Function Prototypes
**************************************************** */
void initChip();
void InterruptHandlerHigh(void);

/*****************************************************
                     Variables
**************************************************** */
static int timer_status = 0;

static enum ir_transmit_state_e ir_transmit_state = IDLE_TRANSMIT;
static enum ir_receive_state_e ir_receive_state = IDLE_RECEIVE;
static unsigned char* buf_ptr = NULL;
static unsigned char buf_num_bytes = 0;
static unsigned char buf_last_bitmask = 0;
static unsigned char my_buffer[] = {0xFF, 0xAA, 0x55};

/*************************************************
  RESET VECTORS: REMOVE IF NOT USING BOOTLOADER!!!
**************************************************/
extern void _startup (void);
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}
#pragma code

#pragma code _HIGH_INTERRUPT_VECTOR = 0x000808
void _high_ISR (void)
{
   InterruptHandlerHigh();
}


#pragma code _LOW_INTERRUPT_VECTOR = 0x000818
void _low_ISR (void)
{
   InterruptHandlerHigh();
}
#pragma code
/* END OF VECTOR REMAPPING*/


/*************************************************
			Initialize the CHIP
**************************************************/
void initChip(){
    char tmp;
    PORTA = 0x00;
    TRISA = 0x00;		//Port A input
    ADCON1 = 0x0f;		//Turn off ADcon
    CMCON = 0x07;		//Turn off Comparator
    PORTB = 0x00;
    TRISB = 0x03;		//Port B output
    PORTC = 0x00;
    TRISC = 0x00;
    INTCONbits.GIE = 0;	// Turn Off global interrupt

    TRISCbits.TRISC2 = 0; //PWM output
    LATCbits.LATC2 = 0;

    // Timer 2 config (for PWM)
    PR2 = 0xD6;
    T2CONbits.T2CKPS = 0b00;    // 1:16 prescaler
    T2CONbits.TMR2ON = 1;       // Turn on timer

    // PWM config
    CCPR1L = 0x6B;
    //CCP1CONbits.CCP1M = 0b1100;   // Turn on PWM mode
    CCP1CONbits.CCP1M = 0b0000;   // Turn off PWM mode

    // Timer 0 config (for PLM)
    T0CONbits.T08BIT = 1;       // Put in 8 bit mode
    T0CONbits.T0CS = 0;         // Select internal clock
    T0CONbits.PSA = 0;          // Prescaler used
    T0CONbits.T0PS = 0b110;     // Prescaler = 1:128
    //T0CONbits.TMR0ON = 1;       // Turn on timer
    T0CONbits.TMR0ON = 0;       // Turn off timer
    
    INTCON2bits.TMR0IP = 1; //T0 generates high priority interrupts

    // Timer 1 config (for IR receiver)
    T1CONbits.RD16 = 0;         // 2 8 bit operations
    T1CONbits.T1CKPS = 0b00;    // Prescaler = 1:1
    T1CONbits.TMR1ON = 1;       // Turn on timer1

    IPR1bits.TMR1IP = 1;        // High priority interrupts
    PIR1bits.TMR1IF = 0;        // Clear interrupt flag
    PIE1bits.TMR1IE = 1;        // Interrupts enabled

    //Interrupts
    INTCONbits.PEIE = 1; //Peripheral interrupts are enabled
    INTCONbits.TMR0IF = 0; //Interrupt flag is cleared
    INTCONbits.TMR0IE = 1; //Interrupts from TMR0 are allowed

    // External interrupt config
    INTCON2bits.INTEDG0 = 1;    // Set RB0 to rising edge detect
    INTCON2bits.INTEDG1 = 0;    // Set RB1 to falling edge detect

    INTCONbits.INT0IF = 0;      // Clear flag
    INTCONbits.INT0IE = 1;      // Enable RB0 interrupt

    INTCON3bits.INT1IF = 0;      // Clear flag
    INTCON3bits.INT1IE = 1;      // Enable RB1 interrupt

    INTCONbits.GIE = 1; //Turn on global interrupts
}

/*********************************************************
	Interrupt Handler
**********************************************************/
#pragma interrupt InterruptHandlerHigh
void InterruptHandlerHigh(void)
{
    static unsigned char buf_cur_byte = 0;
    static unsigned char buf_cur_bit = 128;

    INTCONbits.GIE = 0;
    
    if( INTCONbits.TMR0IF == 1 )
    {
      switch(ir_transmit_state)
      {
        case IDLE_TRANSMIT:
          break;
        case SEND_HEADER:
          TMR0L = TIM0_PRELOAD_2400;
          PLM_ON;
          PWM_ON;
          ir_transmit_state = SEND_SPACE;
          break;
        case SEND_SPACE:
          TMR0L = TIM0_PRELOAD_600;
          PWM_OFF;
          ir_transmit_state = SEND_BIT;
          break;
        case SEND_BIT:
          if(*(buf_ptr + buf_cur_byte) & buf_cur_bit)
            TMR0L = TIM0_PRELOAD_1200;
          else
            TMR0L = TIM0_PRELOAD_600;
          PWM_ON;
          if(buf_cur_bit != 1)
            buf_cur_bit >>= 1;
          else
            buf_cur_bit = 128;
          // Check if we are at the last byte
          if(buf_cur_byte < (buf_num_bytes - 1))
          {
            if(buf_cur_bit == 128)
              buf_cur_byte++;
            ir_transmit_state = SEND_SPACE;
          }
          // Last byte
          else
          {
            // There is still a bit left
            if((buf_last_bitmask & buf_cur_bit) && buf_cur_bit != 128)
              ir_transmit_state = SEND_SPACE;
            else
              ir_transmit_state = FINISH_TRANSMIT;
          }
          break;
        case FINISH_TRANSMIT:
          PLM_OFF;
          PWM_OFF;
          buf_cur_byte = 0;
          buf_cur_bit = 128;  // Start MSB
          ir_transmit_state = IDLE_TRANSMIT;
          break;
        default:
          ir_transmit_state = IDLE_TRANSMIT;
          break;
      }

      INTCONbits.TMR0IF=0;     //CLEAR interrupt flag when done
    }
    // Rising edge interrupt
    else if(INTCONbits.INT0IF == 1)
    {
      LATAbits.LATA5 = 0;
      INTCONbits.INT0IF = 0;      // Reset the interrupt flag
    }
    // Falling edge interrupt
    else if(INTCON3bits.INT1IF == 1)
    {
      LATAbits.LATA5 = 1;
      INTCON3bits.INT1IF = 0;     // Reset the interrupt flag
    }
    else if(PIR1bits.TMR1IF == 1)
    {
      LATBbits.LATB2 = ~LATBbits.LATB2;
      //2500us -> H=0x8A L=0xBE
      //1300us -> H=0xC3 L=0x07
      // 700us -> H=0xDF L=0x2B
      TMR1H = UPPER_BYTE(TIM1_PRELOAD_2500);
      TMR1L = LOWER_BYTE(TIM1_PRELOAD_2500);
      PIR1bits.TMR1IF = 0;
    }
    INTCONbits.GIE = 1; // Reenable global interrupt



    //
    //
    //
    unsigned char tim_high;
    unsigned char tim_low;
    unsigned short int tim_total;
    unsigned int transmit;
    unsigned int bits_received = 0;

    switch(ir_receive_state)
    {
      case IDLE_RECEIVE:
        // Check for a rising edge
        if(RISING_EDGE_CHECK)
        {
          ir_receive_state = START_HDR_TIMER;
          RISING_EDGE_RESET;
          //Fallthrough!
        }
        else
        {
          FALLING_EDGE_RESET;
          RCV_TIMER_RESET;
          break;
        }
      case START_HDR_TIMER:
        TMR1H = UPPER_BYTE(TIM1_PRELOAD_2500);
        TMR1L = LOWER_BYTE(TIM1_PRELOAD_2500);
        ir_receive_state = CHECK_HDR_START_PAUSE_TIMER;
        break;
      case CHECK_HDR_START_PAUSE_TIMER:
        // Overflow/rising edge detected, bad transmission
        if(RCV_TIMER_CHECK || RISING_EDGE_CHECK)
        {
          ir_receive_state = ERROR_RECEIVE;
          RCV_TIMER_RESET;
          RISING_EDGE_RESET;
          goto receive_error;
        }
        // Falling edge detected
        else
        {
          FALLING_EDGE_RESET;
          tim_low = TMR1L;
          tim_high = TMR1H;
          tim_total = ALL_BYTE(tim_high, tim_low);
          // Check if timer is in range
          if(tim_total > TIM1_2350 && tim_total < TIM1_2450 )
          {
            TMR1H = UPPER_BYTE(TIM1_PRELOAD_700);
            TMR1L = LOWER_BYTE(TIM1_PRELOAD_700);
            ir_receive_state = CHECK_PAUSE_START_BIT_TIMER;
          }
          // Problem!
          else
          {
            ir_receive_state = ERROR_RECEIVE;
            goto receive_error;
          }
        }
        break;
      case CHECK_PAUSE_START_BIT_TIMER:
        // Overflow/falling edge detected, bad transmission
        if(RCV_TIMER_CHECK || FALLING_EDGE_CHECK)
        {
          ir_receive_state = ERROR_RECEIVE;
          RCV_TIMER_RESET;
          FALLING_EDGE_RESET;
          goto receive_error;
        }
        // Falling edge detected
        else
        {
          RISING_EDGE_RESET;
          tim_low = TMR1L;
          tim_high = TMR1H;
          tim_total = ALL_BYTE(tim_high, tim_low);
          // Check if timer is in range
          if(tim_total > TIM1_550 && tim_total < TIM1_650 )
          {
            TMR1H = UPPER_BYTE(TIM1_PRELOAD_1300);
            TMR1L = LOWER_BYTE(TIM1_PRELOAD_1300);
            transmit = 0;
            bits_received = 0;
            ir_receive_state = CHECK_BIT_START_PAUSE_TIMER;
          }
          // Problem!
          else
          {
            ir_receive_state = ERROR_RECEIVE;
            goto receive_error;
          }
        }

        break;
      case CHECK_BIT_START_PAUSE_TIMER:
        // Overflow/rising edge detected, bad transmission
        if(RCV_TIMER_CHECK || RISING_EDGE_CHECK)
        {
          ir_receive_state = ERROR_RECEIVE;
          RCV_TIMER_RESET;
          RISING_EDGE_RESET;
          goto receive_error;
        }
        // Falling edge detected
        else
        {
          FALLING_EDGE_RESET;
          tim_low = TMR1L;
          tim_high = TMR1H;
          tim_total = ALL_BYTE(tim_high, tim_low);
          // Check if a "1" was transmitted
          if(tim_total > TIM1_1150 && tim_total < TIM1_1250 )
          {
            TMR1H = UPPER_BYTE(TIM1_PRELOAD_700);
            TMR1L = LOWER_BYTE(TIM1_PRELOAD_700);
            bits_received++;
            transmit <<= 1;
            transmit |= 0x0001;
            ir_receive_state = CHECK_PAUSE_START_BIT_TIMER;
          }
          // Check if a "0" was transmitted
          else if (tim_total > TIM1_550 && tim_total < TIM1_650)
          {
            TMR1H = UPPER_BYTE(TIM1_PRELOAD_700);
            TMR1L = LOWER_BYTE(TIM1_PRELOAD_700);
            bits_received++;
            transmit <<= 1;
            transmit &= 0xFFFE;
            ir_receive_state = CHECK_PAUSE_START_BIT_TIMER;
          }
          // Problem!
          else
          {
            ir_receive_state = ERROR_RECEIVE;
            goto receive_error;
          }
        }
        if(bits_received == 16)
        {
          ir_receive_state = FINISH_RECEIVE;
          goto receive_finish;
        }
        break;
      case ERROR_RECEIVE:
receive_error:
        RCV_TIMER_OFF;
        RCV_TIMER_RESET;
        ir_receive_state = IDLE_RECEIVE;
        break;
      case FINISH_RECEIVE:
receive_finish:
        RCV_TIMER_OFF;
        RCV_TIMER_RESET;
        break;
      default:
        ir_receive_state = IDLE_RECEIVE;
        break;
    }

enum ir_receive_state_e
{
  IDLE_RECEIVE,
  START_HDR_TIMER,
  CHECK_HDR_START_PAUSE_TIMER,
  CHECK_PAUSE_START_BIT_TIMER,
  CHECK_BIT_START_PAUSE_TIMER,
  FINISH_RECEIVE
};
}



void main()
{
  initChip();

  while(1)
  {
    if(ir_transmit_state == IDLE_TRANSMIT)
    {
      // Reset buffers and values
      buf_ptr = my_buffer;
      buf_num_bytes = 3;
      buf_last_bitmask = 0xF0;

      // Trigger interrupts to restart state machine
      ir_transmit_state = SEND_HEADER;
      TRIGGER_T0_INT;
    }
    Delay100TCYx(0);
    Delay100TCYx(0);
  }
}
